﻿using System;

namespace NumbersViewModifer
{
    class Program
    {
        static void Main(string[] args)
        {
            NumbersViewModifer modifer = new NumbersViewModifer();
            string test1 = modifer.ReturnNumberName("Українська", 2147483647.01m);
            Console.WriteLine(test1);
            string test2 = modifer.ReturnNumberName("Українська", 2147483647.20m);
            Console.WriteLine(test2);

            string all = modifer.ReturnNumberName("Українська", 0.01m);
            Console.WriteLine(all);
            
            string al = modifer.ReturnNumberName("English", 0.01m);
            Console.WriteLine(al);

            string alд2 = modifer.ReturnNumberName("English", 2.22m);
            Console.WriteLine(alд2);
            string alд3 = modifer.ReturnNumberName("English", 1.21m);
            Console.WriteLine(alд3);
            
            string test3 = modifer.ReturnNumberName("English", 2147483647.01m);
            Console.WriteLine(test3);
            string test4 = modifer.ReturnNumberName("English", 2147483647.20m);
            Console.WriteLine(test4);
            string test5 = modifer.ReturnNumberName("English", 21234.12m);
            Console.WriteLine(test5);

            Console.ReadKey();
        }
    }
}
