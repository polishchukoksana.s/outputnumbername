using System;
using System.Globalization;

namespace NumbersViewModifer
{
    public class NumbersViewModifer
    {
        public string ReturnNumberName(string language, decimal number)
        {
            string fullStringNumberName = "";
            if (language == "English" || language == "english" || language == "En" || language == "EN")
            {
                fullStringNumberName = StringNumberNameInEnglish(number);
            }
            else if(language == "Українська" || language == "українська" || language == "Укр")
            {
                fullStringNumberName = StringNumberNameInUkrainian(number);
            }

            return fullStringNumberName;
        }

        private string StringNumberNameInUkrainian(decimal number)
        {
            string strNumber = Convert.ToString(number, CultureInfo.InvariantCulture);
            string fullStringNumberName = "";

            string[] oneToTenAdditionAfterSeparator = new[] {"", "одна ", "дві ", "три ", "чотири ", "п'ять ", "шість ", "сім ", "вісім", "дев'ять "};
            string[] oneToTen = new[] { "нуль ", " одна гривня ", " дві гривні ", " три гривні ", " чотири гривні ", " п'ять гривeнь ", " шість гривeнь ", " сім гривeнь ", " вісім гривeнь ", " дев'ять гривeнь "};
            string[] dozens = new[]
                {"", " десять ", " двадцять ", " тридцять ", " сорок ", " п'ядесят ", " шістдесят ", " сімдесят ", " вісімдесят ", " девьносто "}; // 
            string[] hundreds = new[]
            {
                "","одна тисяча ", "дві тисячі ", " три тисячі ", " чотири тисячі ", " п'ять тисяч ", " шість тисяч ",
                " сім тисяч ", " вісім тисяч ", " дев'ять тисяч "
            };
            string[] thousands = new[]
            {
               "", "сто ", " двісті ", " триста ", " чотириста ", " п'ятсот ",
                " шістсот ", " сімсот ", " вісімсот ", " дев'ятсот "
            };
            string[] hundredthousand = new[]
            {
                "","сто тисяч ", " двісті тисяч ", " триста тисяч ", " чотириста тисяч ",
                " п'ятсот тисяч ", " шістсот тисяч ", " сімсот тисяч ", " вісімсот тисяч ",
                " дев'ятсот тисяч "
            };
            string[] millions = new[]
            {
                ""," один мільйон ", "два мільйони ", "три мільйони ", " чотири мільйони ", " п'ять мільйонів ", " шість мільйонів ",
                " сім мільйонів ", " вісім мільйонів ", " дев'ять мільйонів "
            };
            
            string[] billions = new[]
            {
                "","один мільярд ", "два мільярди ", " три мільярди ", " чотири мільярди ", " п'ять мільярдів ", " шість мільярдів ",
                " сім мільярдів ", " вісім мільярдів ", " дев'ять мільярдів "
            };

            bool isSeparator = false;
            int additionAfterSeparator = 0;
            int separator = 0;
            int separatorKeeper = 0;
            int numbersAfterSeparator = 0;
            int iterationCounter = 0;
            int index = 0;

            
            for (int i = 0; i < strNumber.Length; i++)
            {
                if (strNumber[i] == '.' || strNumber[i] == ',' || strNumber[i] == ' '|| strNumber[i] == strNumber.Length + 1)
                {
                    separator += i;
                    separatorKeeper = separator;
                    isSeparator = true;
                }
                else if (separator != 0)
                {
                    for (i = separator + 1; i < strNumber.Length; i++)
                    {
                        numbersAfterSeparator++;
                    }
                    additionAfterSeparator = numbersAfterSeparator;
                }
            }

            for (int i = 0; i < strNumber.Length; i++)
            {
                if (strNumber[strNumber.Length - (numbersAfterSeparator)] == 0)
                {
                    for (int j = numbersAfterSeparator; j < numbersAfterSeparator; j++)
                    {
                        if (strNumber[strNumber.Length - numbersAfterSeparator].ToString() == "0" &&
                            strNumber[strNumber.Length - numbersAfterSeparator - 1].ToString() == "." ||
                            strNumber[strNumber.Length - numbersAfterSeparator].ToString() == "0" &&
                            strNumber[strNumber.Length - numbersAfterSeparator - 1].ToString() == "0")
                        {
                            isSeparator = false;
                        }
                        else
                        {
                            isSeparator = true;
                        }
                    }
                }
                else if (i == separatorKeeper && isSeparator)
                {
                    index = 0;
                }
                else
                {
                    index = Convert.ToInt32(strNumber[i].ToString());
                }

                if (additionAfterSeparator > 0 && isSeparator && index == 0 && separator == 0)
                {
                    for (int j = 0; j < numbersAfterSeparator; j++)
                    {
                        iterationCounter++;
                        if (strNumber[strNumber.Length - numbersAfterSeparator] != 0)
                        {
                            if (iterationCounter == 1)
                            {
                                index = Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator].ToString());
                                fullStringNumberName += dozens[index];
                            }
                            else
                            {
                                index = Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator].ToString());
                                fullStringNumberName += oneToTenAdditionAfterSeparator[index];
                            }
                            additionAfterSeparator--;
                            i++;
                        }
                    }
                }

                if (index == 0 && additionAfterSeparator > 0 || separator == 0)
                {
                    fullStringNumberName += "";
                    separator--;
                }
                else
                {
                    if (separator == 10)
                    {
                        fullStringNumberName += billions[index];
                        separator--;
                    }
                    else if (separator == 9 || separator == 6 || separator == 3)
                    {
                        fullStringNumberName += thousands[index];
                        separator--;
                    }
                    else if (separator == 8 || separator == 5 || separator == 2)
                    {
                        fullStringNumberName += dozens[index];
                        separator--;
                    }
                    else if (separator == 7)
                    {
                        fullStringNumberName += millions[index];
                        separator--;
                    }
                    else if (separator == 4)
                    {
                        fullStringNumberName += hundreds[index];
                        separator--;
                    }
                    else if (separator == 1)
                    {
                        fullStringNumberName += oneToTen[index];
                        separator--;
                    }
                }
            }

            if (isSeparator)
            {
                if(iterationCounter  == 2 && Convert.ToInt32(strNumber[strNumber.Length-1].ToString()) == 1)
                {
                    fullStringNumberName += " копійка";
                }
                else if (iterationCounter  == 1 && Convert.ToInt32(strNumber[strNumber.Length - iterationCounter].ToString()) > 5 || Convert.ToInt32(strNumber[strNumber.Length - 1].ToString()) == 0)
                {
                    fullStringNumberName += " копійок";
                }
                else if (iterationCounter > 1 && Convert.ToInt32(strNumber[strNumber.Length - iterationCounter + 1].ToString()) < 5) 
                {
                    fullStringNumberName += " копійки ";
                }
            }
            return fullStringNumberName;
        }

        private string StringNumberNameInEnglish(decimal number)
        {
            string strNumber = Convert.ToString(number, CultureInfo.InvariantCulture);
            string fullStringNumberName = "";

            string[] oneToten = new[] {"", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine "};
            string[] dozens = new[]
                {"","ten ", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety","", "eleven", "twelve", 
                    "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"  }; // 
            string[] hundreds = new[]
            {
                "","one hundred ", "two hundred ", "three hundred ", "four hundred ", "five hundred ", "six hundred ",
                "seven hundred ", "eight hundred ", "nine hundred "
            };
            string[] thousands = new[]
            {
               "", "one thousand ", "two thousand ", "three thousand ", "four thousand ", "five thousand ",
                "six thousand ", "seven thousand ", "eight thousand ", "nine thousand "
            };
            string[] hundredthousand = new[]
            {
                "","one hundred thousand ", "two hundred thousand ", "three hundred thousand ", "four hundred thousand ",
                "five hundred thousand ", "six hundred thousand ", "seven hundred thousand ", "eight hundred thousand ",
                "nine hundred thousand "
            };
            string[] millions = new[]
            {
                "","one million ", "two millions ", "three millions ", "four millions ", "five millions ", "six millions ",
                "seven millions ", "eight millions ", "nine millions "
            };
            
            string[] billions = new[]
            {
                "","one billion ", "two billions ", "three billions ", "four billions ", "five billions ", "six billions ",
                "seven billions ", "eight billions ", "nine billions"
            };

            bool isSeparator = false;
            int additionAfterSeparator = 0;
            int separator = 0;
            int separatorKeeper = 0;
            int numbersAfterSeparator = 0;
            int iterationCounter = 0;
            int index = 0;
            bool and = false;

            
            for (int i = 0; i < strNumber.Length; i++)
            {
                if (strNumber[i] == '.' || strNumber[i] == ',' || strNumber[i] == ' '|| strNumber[i] == strNumber.Length + 1)
                {
                    separator += i;
                    separatorKeeper = separator;
                    isSeparator = true;
                }
                else if (separator != 0)
                {
                    for (i = separator + 1; i < strNumber.Length; i++)
                    {
                        numbersAfterSeparator++;
                    }
                    additionAfterSeparator = numbersAfterSeparator;
                }
            }

            for (int i = 0; i < strNumber.Length; i++)
            {
                if(strNumber[strNumber.Length - (numbersAfterSeparator)].ToString() == "0")
                {
                    for (int j = 0; j >= numbersAfterSeparator; j++)
                    {
                        if (strNumber[strNumber.Length - numbersAfterSeparator].ToString() == "0")
                        {
                            isSeparator = false;
                        }
                        else
                        {
                            isSeparator = true;
                        }
                    }
                }
                else if (i == separatorKeeper && isSeparator)
                {
                    index = 0;
                }
                if (strNumber[i] == '.' || strNumber[i] == ',' || strNumber[i] == ' ')
                {
                    index = 0;
                    if(strNumber[strNumber.Length - (numbersAfterSeparator-1)].ToString() == "1" && strNumber[separatorKeeper +1] != '0')
                    {
                        fullStringNumberName += "dollar ";
                    }
                    else if (separatorKeeper >= 1 && Convert.ToInt32(strNumber[separatorKeeper-1].ToString()) > 0)
                    {
                        fullStringNumberName += "dollars ";
                    }
                    i++;
                }
                else
                {
                    index = Convert.ToInt32(strNumber[i].ToString());
                }

                if (additionAfterSeparator > 0 && isSeparator && separator == 0)
                {
                    for (int j = 0; j < numbersAfterSeparator; j++)
                    {
                        iterationCounter++;
                        if (strNumber[strNumber.Length - numbersAfterSeparator] != 0)
                        {
                            
                            if (iterationCounter == 1 || iterationCounter == 2 && Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator].ToString()) == 1 )
                            {
                                index = Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator].ToString());
                                if (iterationCounter == 1 && index != 0 && !and || Convert.ToInt32(strNumber[strNumber.Length - 4].ToString()) > 1 && !and)
                                {
                                    fullStringNumberName += "and ";
                                    and = true;
                                }
                                
                                if (iterationCounter == 1 && Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator].ToString()) ==
                                    1 && Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator]
                                        .ToString()) != 0)
                                {
                                    string num = strNumber[strNumber.Length - additionAfterSeparator].ToString();
                                    num += strNumber[strNumber.Length - additionAfterSeparator +1].ToString();
                                    index = Convert.ToInt32(num);
                                    fullStringNumberName += dozens[index];
                                    separator--;
                                    j++;
                                }
                                else if (iterationCounter == 1 &&
                                         Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator]
                                             .ToString()) == 0)
                                {
                                    fullStringNumberName += dozens[index];
                                    separator--;
                                }
                                else if (strNumber[strNumber.Length - additionAfterSeparator-1].ToString() != "." && Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator-1].ToString()) == 0)

                                {
                                    index = Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator]
                                        .ToString());
                                    fullStringNumberName += oneToten[index];
                                    separator--;
                                }
                                else if (additionAfterSeparator == 1 && index != 0)
                                {
                                    fullStringNumberName += oneToten[index];
                                    separator--;
                                    
                                }
                                else
                                {
                                    fullStringNumberName += dozens[index];
                                    separator--;
                                    if (numbersAfterSeparator > 1 && Convert.ToInt32(
                                                                      strNumber[
                                                                              strNumber.Length - (separatorKeeper + 1)]
                                                                          .ToString()) != 0
                                                                  && Convert.ToInt32(strNumber[strNumber.Length - 1]
                                                                      .ToString()) != 0
                                                                  && index != 0 && separator > 0 ||
                                        strNumber[strNumber.Length - additionAfterSeparator - 1].ToString() != "0")
                                    {
                                        fullStringNumberName += "-";
                                    }
                                }
                            }
                            else
                            {
                                if (index != 0 && Convert.ToInt32(strNumber[strNumber.Length - (separatorKeeper + 1)].ToString()) != 0 && !and )
                                {
                                    fullStringNumberName += "and ";
                                    and = true;
                                }
                                else if (iterationCounter < additionAfterSeparator || index > 0)
                                {
                                    index = Convert.ToInt32(strNumber[strNumber.Length - additionAfterSeparator].ToString());
                                    fullStringNumberName += oneToten[index];
                                    separator--;
                                }
                            }
                            additionAfterSeparator--;
                            i++;
                        }
                    }
                }

                if (index == 0 && additionAfterSeparator > 0 || separator == 0)
                {
                    fullStringNumberName += "";
                    separator--;
                }
                else if (separator == 10)
                {
                    fullStringNumberName += billions[index];
                    separator--;
                }
                else if (separator == 8 || separator == 5 || separator == 2)
                {
                    fullStringNumberName += dozens[index];
                    if (separator > 1)
                    {
                        fullStringNumberName += "-";
                    }
                    separator--;
                }
                else if (separator == 6 || separator == 9 || separator == 3)
                {
                    fullStringNumberName += hundreds [index];
                    separator--;
                }
                else if (separator == 7)
                {
                    fullStringNumberName += millions[index];
                    separator--;
                }
                else if (separator == 4)
                {
                    fullStringNumberName += thousands[index];
                    separator--;
                }
                else if (separator == 1)
                {
                    fullStringNumberName += oneToten[index];
                    separator--;
                }
               
            }
            if (isSeparator)
            {
                if (iterationCounter == 2 && strNumber[strNumber.Length - (numbersAfterSeparator-1)].ToString() == "1" )
                {
                    fullStringNumberName += "cent";
                }
                else if (iterationCounter == 1 || strNumber[strNumber.Length - (numbersAfterSeparator)].ToString() != "1" || strNumber[separatorKeeper+1].ToString() == "1") 
                {
                    fullStringNumberName += " cents ";
                }
            }
            return fullStringNumberName;
        }
    }
}
